import requests
from resources.constants import Login
from BaseClass.baseclass import BaseCase
from BaseClass.assertions import Assertions
import pytest


class TestLogin(BaseCase):
    url = Login.url
    user_data = Login.correct_user_data
    expected_token = Login.token_as_json.get('token')

    def setup(self):
        self.correct_response = requests.post(self.url, data=self.user_data)

    def test_response_is_ok(self):
        """Checks if response status_code is ok"""

        assert self.correct_response.status_code == 200, 'Response status code should be equal to 200'

    def test_token_in_response(self):
        """compares two values"""

        Assertions.assert_json_value_by_name(
            self.correct_response,
            'token',
            self.expected_token,
            'Response token value should be equal to expected token value.'
        )

    @pytest.mark.parametrize(
        "test_input,expected",
        [({'email': 'only@email.com'}, 'Missing password'),
         ({'password': 'the_only_password'}, "Missing email or username"),
         pytest.param({"email": "eve.holt@reqres.in",
                       "password": "pistol"}, 42, marks=pytest.mark.xfail)]
    )
    def test_got_error_message_in_bad_response(self, test_input, expected):
        """Wrong data input. Test compares actual error message with expected error message"""

        incorrect_response = requests.post(self.url, data=test_input)
        Assertions.assert_json_value_by_name(
            incorrect_response,
            'error',
            expected,
            "We should have an error message is response."
        )
