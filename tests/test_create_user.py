import requests
from resources.constants import Register
from BaseClass.baseclass import BaseCase
from BaseClass.assertions import Assertions


class TestRegister(BaseCase):
    url = Register.url
    new_user_data = Register.new_user_data
    bad_data = Register.bad_new_data
    expected_token = Register.token_as_json.get('token')
    expected_error = Register.expected_error_as_json.get('error')

    def setup(self):
        self.ok_response = requests.post(self.url, data=self.new_user_data)
        self.not_ok_response = requests.post(self.url, data=self.bad_data)

    def test_register_new_user_is_ok(self):
        """Checks if response status_code is ok"""

        assert self.ok_response.ok

    def test_get_new_user_name(self):
        """Checks if we have token in response"""

        Assertions.assert_json_value_by_name(
            self.ok_response,
            'token',
            self.expected_token,
            'Token value should be equal to expected token value.'
        )

    def test_register_new_user_with_bad_data(self):
        """Checks if status code is not ok."""

        assert self.not_ok_response.status_code == 400

    def test_got_error_message_in_bad_response(self):
        """Checks if we have error message in response"""

        Assertions.assert_json_value_by_name(
            self.not_ok_response,
            'error',
            self.expected_error,
            "We should have an error message is response."
        )
