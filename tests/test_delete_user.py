import random
import requests
from resources.constants import Delete
from BaseClass.baseclass import BaseCase


class TestDelete(BaseCase):
    users_url = 'https://reqres.in/api/users?page='

    def setup(self):
        """Method gets the list of users ids"""

        self.users_response = requests.get(self.users_url)

        # проверяем, что в ответе есть total_pages (количество страниц с айди пользователей)
        json_value = self.get_json_value(self.users_response, 'total_pages')

        # забираем значение количества страниц
        pages_number = self.get_num_of_pages(json_value)

        # полный список айди пользователей
        self.total_users_id_list = self.get_list_of_users_id(self.users_url, pages_number)

    def test_delete(self):
        """In this test we try to delete random user
        Вообще-то delete срабатывает даже на пустое значение"""

        random_user_to_delete = random.choice(self.total_users_id_list)

        delete_url = Delete.url + f'{random_user_to_delete}'
        response = requests.delete(delete_url)

        assert response.status_code == 204, f"Can\'t delete {random_user_to_delete}. Looks like we are trying to send " \
                                            f"request to the wrong url."


