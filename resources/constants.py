class Register:
    url = 'https://reqres.in/api/register'

    new_user_data = {
        "email": "eve.holt@reqres.in",
        "password": "pistol"
    }

    bad_new_data = {'some shit': 'even more shit'}

    token_as_json = {"token": "QpwL5tke4Pnpja7X4"}

    expected_error_as_json = {"error": "Missing email or username"}


class Login:
    url = 'https://reqres.in/api/login'

    correct_user_data = {
        "email": "eve.holt@reqres.in",
        "password": "cityslicka"
    }

    token_as_json = {"token": "QpwL5tke4Pnpja7X4"}


class Delete:

    url = 'https://reqres.in/api/users/'
