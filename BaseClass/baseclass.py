import json.decoder

import requests
from requests import Response


class BaseCase:

    def get_cookie(self, response: Response, cookie_name):
        assert cookie_name in response.cookies, f'Can not find cookie with name \'{cookie_name}\' in the last response.'

        return response.cookies[cookie_name]

    def get_header(self, response: Response, header_name):
        assert header_name in response.headers, f'Cannot find header with name {header_name} in the last response'
        return response.headers[header_name]

    def get_json_value(self, response: Response, name):

        try:
            response_as_dict = response.json()

        except json.decoder.JSONDecodeError:
            assert False, f"Response is not in JSON Format. Response text is {response.text}"

        assert name in response_as_dict, f"Response JSON doesn't have key '{name}' "

        return response_as_dict

    def get_num_of_pages(self, json_dict):
        """Returns total number of pages with users"""
        try:
            number = json_dict['total_pages']
        except TypeError:
            assert False, f"Response is not in Needed Format. Response text is {json_dict}"

        return int(number)

    def get_list_of_users_id(self, url, number_of_pages):
        """"""
        list_of_users_id = []

        for i in range(1, number_of_pages + 1):

            response = requests.get(url=url + f'{i}')
            page_list_of_users = self.get_json_value(response=response, name='data')['data']
            for element in page_list_of_users:
                list_of_users_id.append(element['id'])

        return list_of_users_id
